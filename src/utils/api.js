import request from './request'
import qs from 'qs'


/** post 请求
 * @param  {请求参数} params
 */
export const User = param => request.post('/User', param)
// export const User = param => {
// 	return new Promise((resolve, reject) => {
// 		axios.post('/User', { params })
// 			.then(response => {
// 				resolve(response.data)
// 			})
// 			.catch(error => {
// 				reject(error)
// 			})
// 	})
// }

// 微信登录（这个接口必须用qs对数据进行格式化）
export const WeixinLoginApi = params => request.post('/wechatUsers/PCLogin', qs.stringify(params))
