import axios from 'axios'

let instance = axios.create({
	baseURL: 'https://95c0f55e-b06f-4756-9366-1a1f430ede29.bspapp.com',
	timeout: 5000
})

// http request 拦截器
instance.interceptors.request.use(
	config => {
		if (config.url === '/wechatUsers/PCLogin') {
			config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
		}
		const token = sessionStorage.getItem('token')
		if (token) {
			// 判断是否存在token，如果存在的话，则每个http header都加上token
			config.headers['x-auth-token'] = token //请求头加上token
		}
		return config
	},
	err => {
		return Promise.reject(err)
	}
)

// http response 拦截器
instance.interceptors.response.use(
	response => {
		return response.data
	},
	//接口错误状态处理，也就是说无响应时的处理
	error => {
		return Promise.reject(error.response.status) // 返回接口返回的错误信息
	}
)

export default instance
