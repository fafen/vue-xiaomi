import Vue from 'vue'
import App from './App.vue'

// ViewUI
import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'
Vue.use(ViewUI)

// router
import VueRouter from './router'

// vuex
import store from './store'

// reset-css
import 'reset-css'

// 拼图验证码
import SlideVerify from 'vue-monoplasty-slide-verify' 
Vue.use(SlideVerify)

// element
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

new Vue({
  el: '#app',
  router: VueRouter,
  store,
  render: h => h(App)
})
