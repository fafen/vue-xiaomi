import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home.vue'
import Goods from '../views/Goods.vue'

Vue.use(VueRouter)

export default new VueRouter({
	routes: [{
			//默认首页
			path: '/',
			redirect: '/home'
		},
		{
			path: '/home',
			name: 'Home',
			component: Home
		}, {
			path: '/goods',
			name: 'Goods',
			component: Goods
		}
	]
})
